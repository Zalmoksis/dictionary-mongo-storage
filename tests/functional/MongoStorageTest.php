<?php

namespace Zalmoksis\Dictionary\Storage\Mongo\Tests\Functional;

use MongoDB\Driver\Manager as MongoClient;
use PHPUnit\Framework\TestCase;
use TypeError;
use Zalmoksis\Dictionary\Model\{Category, Entry, Headword, HomographIndex, Translation};
use Zalmoksis\Dictionary\Model\Collections\{Categories, Headwords, Translations};
use Zalmoksis\Dictionary\Parser\ArrayParser\{DefaultArrayDeserializer, DefaultArraySerializer, DefaultArrayStructure};
use Zalmoksis\Dictionary\Storage\{EntriesIndexedById, EntryRepository, Mongo\EntryMongoRepository};

final class MongoStorageTest extends TestCase {
    public EntryMongoRepository $storage;

    function setUp(): void {
        $mongoHost     = getenv('MONGO_HOST')     ?: 'localhost';
        $mongoPort     = getenv('MONGO_PORT')     ?: '27017';
        $mongoDatabase = getenv('MONGO_DATABASE') ?: 'mongo_dictionary_functional_tests';

        try {
            $this->storage = new EntryMongoRepository(
                new MongoClient("mongodb://{$mongoHost}:{$mongoPort}"),
                $mongoDatabase,
                new DefaultArraySerializer(new DefaultArrayStructure()),
                new DefaultArrayDeserializer(new DefaultArrayStructure())
            );
        } catch (TypeError $error) {
            $this->storage = new EntryMongoRepository(
                new MongoClient("mongodb://{$mongoHost}:{$mongoPort}"),
                $mongoDatabase,
                new DefaultArraySerializer(),
                new DefaultArrayDeserializer()
            );
        }

        $this->storage->drop();
    }

    function tearDown(): void {
        $this->storage->drop();
    }

    function testInterface(): void {
        static::assertInstanceOf(EntryRepository::class, $this->storage);
    }

    function testInsertingEntry(): void {
        static::assertSame(
            'cat',
            $this->storage->save(
                (new Entry())
                    ->setHeadwords(new Headwords(new Headword('cat')))
                    ->setCategories(new Categories(new Category('n')))
                    ->setTranslations(new Translations(new Translation('kot')))
            )
        );

        static::assertEquals(
            new EntriesIndexedById([
                'cat' => (new Entry())
                    ->setHeadwords(new Headwords(new Headword('cat')))
                    ->setCategories(new Categories(new Category('n')))
                    ->setTranslations(new Translations(new Translation('kot')))
            ]),
            $this->storage->findByHeadword('cat')
        );

        static::assertEquals(
            (new Entry())
                ->setHeadwords(new Headwords(new Headword('cat')))
                ->setCategories(new Categories(new Category('n')))
                ->setTranslations(new Translations(new Translation('kot'))),
            $this->storage->findById('cat')
        );
    }

    function testInsertingMultipleEntriesWithSameId(): void {
        static::assertSame(
            'cat',
            $this->storage->save(
                (new Entry())
                    ->setHeadwords(new Headwords(new Headword('cat')))
                    ->setCategories(new Categories(new Category('n')))
                    ->setTranslations(new Translations(new Translation('kot')))
            )
        );

        static::assertSame(
            'cat',
            $this->storage->save(
                (new Entry())
                    ->setHeadwords(new Headwords(new Headword('cat')))
                    ->setCategories(new Categories(new Category('n')))
                    ->setTranslations(new Translations(new Translation('die Katze')))
            )
        );

        static::assertEquals(
            new EntriesIndexedById([
                'cat' => (new Entry())
                    ->setHeadwords(new Headwords(new Headword('cat')))
                    ->setCategories(new Categories(new Category('n')))
                    ->setTranslations(new Translations(new Translation('die Katze')))
            ]),
            $this->storage->findByHeadword('cat')
        );

        static::assertEquals(
            (new Entry())
                ->setHeadwords(new Headwords(new Headword('cat')))
                ->setCategories(new Categories(new Category('n')))
                ->setTranslations(new Translations(new Translation('die Katze'))),
            $this->storage->findById('cat')
        );
    }

    function testInsertingMultipleEntriesWithSameHeadwordAndDifferentHomographIndexes(): void {
        static::assertSame(
            'light.1',
            $this->storage->save(
                (new Entry())
                    ->setHeadwords(new Headwords(new Headword('light')))
                    ->setHomographIndex(new HomographIndex(1))
                    ->setCategories(new Categories(new Category('n')))
                    ->setTranslations(new Translations(new Translation('światło')))
            )
        );

        static::assertSame(
            'light.2',
            $this->storage->save(
                (new Entry())
                    ->setHeadwords(new Headwords(new Headword('light')))
                    ->setHomographIndex(new HomographIndex(2))
                    ->setCategories(new Categories(new Category('adj')))
                    ->setTranslations(new Translations(new Translation('lekki')))
            )
        );

        static::assertEquals(
            new EntriesIndexedById([
                'light.1' => (new Entry())
                    ->setHeadwords(new Headwords(new Headword('light')))
                    ->setHomographIndex(new HomographIndex(1))
                    ->setCategories(new Categories(new Category('n')))
                    ->setTranslations(new Translations(new Translation('światło'))),
                'light.2' => (new Entry())
                    ->setHeadwords(new Headwords(new Headword('light')))
                    ->setHomographIndex(new HomographIndex(2))
                    ->setCategories(new Categories(new Category('adj')))
                    ->setTranslations(new Translations(new Translation('lekki')))
            ]),
            $this->storage->findByHeadword('light')
        );

        static::assertEquals(
            (new Entry())
                ->setHeadwords(new Headwords(new Headword('light')))
                ->setHomographIndex(new HomographIndex(1))
                ->setCategories(new Categories(new Category('n')))
                ->setTranslations(new Translations(new Translation('światło'))),
            $this->storage->findById('light.1')
        );

        static::assertEquals(
            (new Entry())
                ->setHeadwords(new Headwords(new Headword('light')))
                ->setHomographIndex(new HomographIndex(2))
                ->setCategories(new Categories(new Category('adj')))
                ->setTranslations(new Translations(new Translation('lekki'))),
            $this->storage->findById('light.2')
        );
    }

    function testFindingMissingEntry(): void {
        $this->storage->save(
            (new Entry())
                ->setHeadwords(new Headwords(new Headword('cat')))
                ->setCategories(new Categories(new Category('n')))
                ->setTranslations(new Translations(new Translation('kot')))
        );

        static::assertEquals(
            new EntriesIndexedById(),
            $this->storage->findByHeadword('light')
        );

        static::assertEquals(
            null,
            $this->storage->findById('light')
        );
    }

    function testFindingAllHeadwords(): void {
        // result is sorted and includes secondary headwords as well

        $this->storage->save(
            (new Entry())
                ->setHeadwords(new Headwords(new Headword('prison')))
        );
        $this->storage->save(
            (new Entry())
                ->setHeadwords(new Headwords(
                    new Headword('jail'),
                    new Headword('gaol')
                ))
        );

        $this->assertEquals(
            new Headwords(
                new Headword('gaol'),
                new Headword('jail'),
                new Headword('prison'),
            ),
            $this->storage->findHeadwords()
        );
    }

    function testFindingPaginatedHeadwords(): void {
        // result is sorted, includes secondary headwords and pagination takes all of them into account

        $this->storage->save((new Entry())->setHeadwords(new Headwords(new Headword('headword 4'))));
        $this->storage->save((new Entry())->setHeadwords(new Headwords(new Headword('headword 2'))));
        $this->storage->save(
            (new Entry())
                ->setHeadwords(new Headwords(
                    new Headword('headword 3a'),
                    new Headword('headword 3b'),
                ))
        );
        $this->storage->save((new Entry())->setHeadwords(new Headwords(new Headword('headword 1'))));
        $this->storage->save(
            (new Entry())
                ->setHeadwords(new Headwords(
                    new Headword('headword 5a'),
                    new Headword('headword 5b'),
                ))
        );

        $this->assertEquals(
            new Headwords(
                new Headword('headword 3b'),
                new Headword('headword 4'),
                new Headword('headword 5a'),
            ),
            $this->storage->findHeadwords(3, 2)
        );
    }

    function testIteratingOverEntries(): void {
        $this->storage->save((new Entry())->setHeadwords(new Headwords(new Headword('entry 1'))));
        $this->storage->save((new Entry())->setHeadwords(new Headwords(new Headword('entry 2'))));
        $this->storage->save(
            (new Entry())
                ->setHeadwords(new Headwords(new Headword('entry 3')))
                ->setHomographIndex(new HomographIndex(1))
        );
        $this->storage->save(
            (new Entry())
                ->setHeadwords(new Headwords(new Headword('entry 3')))
                ->setHomographIndex(new HomographIndex(2))
        );

        $this->assertEquals(
            [
                'entry 1' => (new Entry())->setHeadwords(new Headwords(new Headword('entry 1'))),
                'entry 2' => (new Entry())->setHeadwords(new Headwords(new Headword('entry 2'))),
                'entry 3.1' => (new Entry())
                    ->setHeadwords(new Headwords(new Headword('entry 3')))
                    ->setHomographIndex(new HomographIndex(1)),
                'entry 3.2' => (new Entry())
                    ->setHeadwords(new Headwords(new Headword('entry 3')))
                    ->setHomographIndex(new HomographIndex(2)),
            ],
            iterator_to_array($this->storage->getIterator())
        );
    }

    function testDeletingById(): void {
        $entryId = $this->storage->save(
            (new Entry())
                ->setHeadwords(new Headwords(new Headword('cat')))
                ->setCategories(new Categories(new Category('n')))
                ->setTranslations(new Translations(new Translation('kot')))
        );

        $this->storage->deleteById($entryId);

        self::assertNull($this->storage->findById($entryId));
    }

    function testDeletingByIdWhenNotSaved(): void {
        $entryId = 'cat';

        $this->storage->deleteById($entryId);

        self::assertNull($this->storage->findById($entryId));
    }

    function testDropping(): void {
        $entryId = $this->storage->save(
            (new Entry())
                ->setHeadwords(new Headwords(new Headword('cat')))
                ->setCategories(new Categories(new Category('n')))
                ->setTranslations(new Translations(new Translation('kot')))
        );

        $this->storage->drop();

        static::assertEquals(
            new EntriesIndexedById(),
            $this->storage->findByHeadword('cat')
        );

        static::assertNull($this->storage->findById($entryId));
    }
}
