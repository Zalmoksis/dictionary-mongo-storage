# To do

## Fixes and refactoring
Currently empty. 

## Minor
- `MongoStorage` should probably be renamed `MongoCollection`
- `DictionaryMongoStorage` should probably be renamed to `EntryMongoCollection`

## Major
Nothing until 1.0.
