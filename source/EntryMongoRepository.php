<?php

namespace Zalmoksis\Dictionary\Storage\Mongo;

use Generator;
use MongoDB\Driver\{BulkWrite, Command, Manager as Mongo, Query};
use RuntimeException;
use stdClass;
use Zalmoksis\Dictionary\Model\{Collections\Headwords, Entry, Headword};
use Zalmoksis\Dictionary\Parser\ArrayParser\{ArrayDeserializer, ArraySerializer,};
use Zalmoksis\Dictionary\Storage\{EntriesIndexedById, EntryRepository};
use Zalmoksis\Dictionary\Storage\Mongo\Exceptions\DictionaryMongoStorageException;

final class EntryMongoRepository extends MongoStorage implements EntryRepository {
    private ArraySerializer $arraySerializer;
    private ArrayDeserializer $arrayDeserializer;

    protected const COLLECTION = 'entries';

    function __construct(
        Mongo $mongo,
        string $database,
        ArraySerializer $arraySerializer,
        ArrayDeserializer $arrayDeserializer
    ) {
        parent::__construct($mongo, $database);

        $this->arraySerializer = $arraySerializer;
        $this->arrayDeserializer = $arrayDeserializer;
    }

    function save(Entry $entry): string {
        $entryId = $this->getEntryId($entry);

        $bulkWrite = new BulkWrite();
        $bulkWrite->update(
            [
                '_id' => $entryId,
            ],
            $this->arraySerializer->serializeEntry($entry),
            [
                'upsert' => true,
            ]
        );

        $this->executeBulkWrite($bulkWrite);

        return $entryId;
    }

    function findById(string $id): ?Entry {
        $entryDocument = $this->queryForEntries(['_id' => $id])[0] ?? null;

        return null !== $entryDocument
            ? $this->arrayDeserializer->deserializeEntry($entryDocument)
            : null;
    }

    function findByHeadword(string $headword): EntriesIndexedById {
        // TODO: 'headwords' is serialization dependent
        $entries = array_map(
            fn (array $entryData) => $this->arrayDeserializer->deserializeEntry($entryData),
            $this->queryForEntries(['headwords' => $headword])
        );

        return new EntriesIndexedById(array_combine(
            array_map(
                fn (Entry $entry) => $this->getEntryId($entry),
                $entries
            ),
            $entries
        ));
    }

    function findHeadwords($limit = 0, $page = 1): Headwords {
        $pipeline = [
            ['$unwind' => '$headwords'],
            ['$group' => ['_id' => '$headwords']],
            ['$sort' => ['_id' => 1]],
        ];

        if ($limit) {
            $pipeline = [
                ...$pipeline,
                ['$skip' => ($page - 1) * $limit],
                ['$limit' => $limit],
            ];
        }

        $cursor = $this->executeCommand(new Command([
            'aggregate' => self::COLLECTION,
            'pipeline' => $pipeline,
            'cursor' => new stdClass(),
        ]));

        return new Headwords(...array_map(
            fn ($document) => new Headword($document['_id']),
            $cursor->toArray()
        ));
    }

    function getIterator(): Generator {
        $cursor = $this->executeQuery(new Query([]));

        foreach ($cursor as $document) {
            $entry = $this->arrayDeserializer->deserializeEntry($document);

            yield $this->getEntryId($entry) => $entry;
        }
    }

    function deleteById(string $entryId): void {
        $bulkWrite = new BulkWrite();
        $bulkWrite->delete(['_id' => $entryId]);

        $this->executeBulkWrite($bulkWrite);
    }

    function drop(): void {
        try {
            $this->executeCommand(new Command([
                'drop' => self::COLLECTION,
            ]));
        } catch (RuntimeException $exception) {
            if ($exception->getMessage() == 'ns not found') {
                return;
            }
            throw new DictionaryMongoStorageException(
                'Error while dropping "' . self::COLLECTION . '" collection',
                0,
                $exception
            );
        }
    }

    private function getEntryId(Entry $entry): string {
        // a naive temporary implementation
        // issues:
        // - casing: uppercase is different to lowercase

        // TODO: this logic should perhaps be included in the Dictionary domain model
        $entryId = $entry->getHeadwords()->getFirst()->getValue();

        if ($entry->getHomographIndex()) {
            $entryId .= '.' . $entry->getHomographIndex()->getValue();
        }

        return $entryId;
    }

    private function queryForEntries(array $query, int $limit = 0, int $page = 1): array {
        try {
            $options = [];

            if ($limit > 0) {
                $options['limit'] = $limit;
                $options['skip'] = ($page - 1) * $limit;
            }

            $cursor = $this->executeQuery(new Query($query, $options));

            $entryDocuments = $cursor->toArray();
        } catch (RuntimeException $exception) {
            throw new DictionaryMongoStorageException(
                'Error while executing a MongoDB query',
                0,
                $exception
            );
        }

        return $entryDocuments;
    }
}
