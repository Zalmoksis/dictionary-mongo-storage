<?php

namespace Zalmoksis\Dictionary\Storage\Mongo;

use MongoDB\Driver\{BulkWrite, Command, Cursor, Manager as Mongo, Query, WriteResult};

abstract class MongoStorage {
    protected Mongo $mongo;
    protected string $database;

    protected const COLLECTION = '';

    function __construct(Mongo $mongo, string $database) {
        $this->mongo = $mongo;
        $this->database = $database;
    }

    protected function executeCommand(Command $command): Cursor {
        $cursor = $this->mongo->executeCommand(
            $this->database,
            $command
        );

        $this->prepareCursor($cursor);

        return $cursor;
    }

    protected function executeBulkWrite(BulkWrite $bulkWrite): WriteResult {
        return $this->mongo->executeBulkWrite(
            $this->database . '.' . static::COLLECTION,
            $bulkWrite
        );
    }

    protected function executeQuery(Query $query): Cursor {
        $cursor = $this->mongo->executeQuery(
            $this->database . '.' . static::COLLECTION,
            $query
        );

        $this->prepareCursor($cursor);

        return $cursor;
    }

    private function prepareCursor(Cursor $cursor): void {
        $cursor->setTypeMap([
            'root' => 'array',
            'document' => 'array'
        ]);
    }
}
