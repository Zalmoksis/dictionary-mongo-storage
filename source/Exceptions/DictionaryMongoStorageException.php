<?php

namespace Zalmoksis\Dictionary\Storage\Mongo\Exceptions;

use RuntimeException;
use Zalmoksis\Dictionary\Storage\Exception\DictionaryStorageException;

final class DictionaryMongoStorageException extends RuntimeException implements DictionaryStorageException {}
